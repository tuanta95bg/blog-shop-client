import React from "react";
import HomePage from "./Pages/HomePage";
import NotFoundPage from "./Pages/NotFoundPage";
import ProductListPage from "./Pages/ProductListPage";
import ProductActionPage from "./Pages/ProductActionPage";

const routers = [
    {
        path: '/',
        exact: true,
        main: () => <HomePage />
    },
    {
        path: '/admin/product-list',
        exact: false,
        main: () => <ProductListPage />
    },
    {
        path: '/admin/product-add',
        exact: false,
        main: () => <ProductActionPage />
    },
    {
        path: '',
        exact: false,
        main: () => <NotFoundPage />
    }
];

export default routers;