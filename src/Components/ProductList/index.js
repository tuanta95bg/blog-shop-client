import React from "react";
import ShowProductItem from "../../Pages/MapPage/ShowProductItem";

function ProductList() {
    return (
        <div className="card">
            <div className="card-header fw-bolder">
                List of products
            </div>
            <div className="card-body">
                <table className="table table-hover align-middle">
                    <thead className="table-dark">
                    <tr>
                        <th scope="col" className="text-center">#</th>
                        <th scope="col" className="text-start">Name</th>
                        <th scope="col" className="text-center">Price</th>
                        <th scope="col" className="text-center">Status</th>
                        <th scope="col" className="text-end">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <ShowProductItem />
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default ProductList;