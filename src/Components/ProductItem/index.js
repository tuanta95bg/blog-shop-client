import React from "react";

function ProductItem({product, index}) {
    let statusClass = product.status ? "bg-primary" : "bg-danger";
    let statusName = product.status ? "Show" : "Hide";

    return (
        <tr>
            <th scope="row" className="text-center">{index+1}</th>
            <td className="text-start">{product.name}</td>
            <td className="text-center">{product.price}</td>
            <td className="text-center">
                <span className={`badge ${statusClass} text-wrap`}>
                    { statusName }
                </span>
            </td>
            <td className="text-end">
                <button type="button" className="btn btn-success me-1">Sửa</button>
                <button type="button" className="btn btn-danger ms-1">Xóa</button>
            </td>
        </tr>
    )
}

export default ProductItem;