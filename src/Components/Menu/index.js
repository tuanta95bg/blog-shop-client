import React from "react";

function Menu() {
    return (
        <ul className="nav bg-light mb-5">
            <li className="nav-item">
                <a className="nav-link" href="/">Active</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="/">Link</a>
            </li>
        </ul>
    )
}

export default Menu;