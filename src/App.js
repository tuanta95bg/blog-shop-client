import React from "react";
import Menu from "./Components/Menu";
import ShowContentMenu from "./Pages/MapPage/ShowContentMenu";
import {BrowserRouter as Router} from "react-router-dom"

function App() {
  return (
      <div className="App">
          <Router>
              <Menu />
              <div className="container">
                  <ShowContentMenu />
              </div>
          </Router>
      </div>
  );
}

export default App;
