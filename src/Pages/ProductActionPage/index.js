import React, {useState} from "react";

function ProductActionPage() {
    const [image, setImage] = useState('');
    const uploadImage = (file) => {
       setImage(URL.createObjectURL(file))
    }
    return (
        <form className="row">
            <div className="col-8">
                <div className="mb-3">
                    <label htmlFor="title_input" className="form-label">Title</label>
                    <input type="text" className="form-control" id="title_input" />
                </div>
                <div className="mb-3">
                    <label htmlFor="description_input" className="form-label">Description</label>
                    <textarea className="form-control" id="description_input" rows="3"></textarea>
                </div>
                <div className="mb-3">
                    <label htmlFor="price_input" className="form-label">Price</label>
                    <input type="number" className="form-control" id="price_input" />
                </div>
                <div className="form-check mb-3">
                    <input className="form-check-input" type="checkbox" value="" id="checkbox_input" />
                    <label className="form-check-label" htmlFor="checkbox_input">
                        Show products outside the store
                    </label>
                </div>
            </div>
            <div className="col-4">
                <div className="mb-3">
                    <label htmlFor="image_input" className="form-label">Image</label>
                    <input className="form-control" type="file" id="image_input"
                           onChange={(e)=>uploadImage(e.target.files[0])}
                    />
                </div>
                <img src={image} alt={image} style={{ width:'100%' }}/>
            </div>
            <div className="col-12">
                <button type="submit" className="btn btn-primary">Submit</button>
            </div>
        </form>
    )
}

export default ProductActionPage;