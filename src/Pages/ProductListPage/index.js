import React from "react";
import ProductList from "../../Components/ProductList";

function ProductListPage() {
    return (
        <div className="row">
            <div className="col-12">
                <button type="button" className="btn btn-primary mb-3">Product new</button>
                <ProductList/>
            </div>
        </div>
    )
}

export default ProductListPage;