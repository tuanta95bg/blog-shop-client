import React from "react";
import ProductItem from "../../Components/ProductItem";

let products = [
    {
        id: 1,
        name: "Mark",
        price: 500,
        description: "aaaaaaaaaaaaaaaaaaaaaaaa",
        status: true
    },
    {
        id: 2,
        name: "Mark 2",
        price: 1000,
        description: "bbbbbbbbbbbbbbbbbbbbb",
        status: false
    },
    {
        id: 3,
        name: "Mark 3",
        price: 10,
        description: "ccccccccccccccccccccccc",
        status: false
    }
];
// export default showProductItem;
const ShowProductItem = () => {
    let result = null;
    if (products.length > 0) {
        result = products.map((product, index)=>{
            return (
                <ProductItem
                    key={index}
                    index={index}
                    product={product}
                />
            )
        })
    }
    return result;
}
export default ShowProductItem;