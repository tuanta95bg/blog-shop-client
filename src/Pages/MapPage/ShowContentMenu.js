import React from "react";
import routers from "./../../routers";
import {Route, Switch} from "react-router-dom";

// export default ShowContentMenu;
const ShowContentMenu = () => {
    let result = null;
    if (routers.length > 0) {
        result = routers.map((route, index)=>{
            return (
                <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    component={route.main}
                />
            )
        })
    }
    return <Switch>{result}</Switch>
}
export default ShowContentMenu;